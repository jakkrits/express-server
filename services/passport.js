const passport = require('passport');
const User = require('../models/user');
const config = require('../config');
const jwtStrategy = require('passport-jwt').Strategy;
const extractJwt = require('passport-jwt').ExtractJwt;
const LocalStrategy = require('passport-local');

//create local strategy
const localLogin = new LocalStrategy({usernameField: 'email'}, function(email, password, done) {
    //verify this email & passport, call done with the user if credentials correct
    //otherwise call done with false
    User.findOne({email: email}, function (err, user) {
       if(err) {return done(err)}
       if(!user) {return done(null, false);}

       //compare passwords - is 'password' equal to user.password
        //decode encrypted password to compare
        user.comparePassword(password, function (err, isAMatch) {
            if(err) {return done(err);}
            if(!isAMatch) {return done(null, false);}

            return done(null, user);
        })
    });
});

//setup options for JWT strategy
const jwtOptions = {
    jwtFromRequest: extractJwt.fromHeader('authorization'),
    secretOrKey: config.secret
};

//create jwt strategy
const jwtLogin = new jwtStrategy(jwtOptions, function (payload, done) {
    //see if the user id in the payload exists in our database
    //if it does, call 'done' with that other
    //otherwise, call done without a user object
    User.findById(payload.sub, function (err, user) {
        if(err) {return done(err, false);}
        if(user) {
            done(null, user);
        } else {
            //user not found -> return false
            done(null, false);
        }
    })
});

//tell passport to use this staregy
passport.use(jwtLogin);
passport.use(localLogin);