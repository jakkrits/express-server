/**
 * Created by Jakk on 9/8/2016 AD.
 */
const express = require('express');
const http = require('http');
const bodyParser = require('body-parser');
const morgan = require('morgan');
const mongoose = require('mongoose');
const router = require('./router');
const cors = require('cors');

const app = express();

//DB Setup
mongoose.connect('mongodb://localhost:auth-db/auth');


//APP SETUP
//Morgan - logging incoming request framework
app.use(morgan('combined'));

//use CORS
app.use(cors());

//BodyParser - parse incoming request as json
app.use(bodyParser.json({type: '*/*'}));

router(app);

//SERVER SETUP
const port = process.env.PORT || 3090;
const server = http.createServer(app);
server.listen(port);
console.log('Server is listening on: ☯', port);