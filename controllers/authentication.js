const jwt = require('jwt-simple');
const userModel = require('../models/user');
const config = require('../config');

//sub = subject
//iat = issued at time
function tokenForUser(user) {
    const timeStamp = new Date().getTime();
    return jwt.encode({
        sub: user.id,
        iat: timeStamp
    }, config.secret);
}

exports.signup = function (req, res, next) {
    const email = req.body.email;
    const password = req.body.password;

    if (!email || !password) {
        return res.status(422).send({error: 'must have username & password'});
    }

    //see if user with given email exists
    userModel.findOne({email: email}, function (err, existingUser) {
        if (err) {
            return next(err);
        }

        //if a user does exist return error
        if (existingUser) {
            return res.status(422).send({error: 'Email alread exists'})
        }

    });

    //if a user not exist, create and save record
    const user = new userModel({
        email: email,
        password: password
    });
    user.save(function (err) {
        if (err) {
            return next(err);
        }
        //respond to request indicating user was created
        res.json({token: tokenForUser(user)});

    });

};

exports.signin = function (req, res, next) {
    //user has already auth'd
    //need to give him a token
    res.send({token: tokenForUser(req.user)});
    //req.user from passport.js (in done function callback)
};